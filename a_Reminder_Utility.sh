#!/bin/bash
#
#I just copied the prompt and closed the book
#
#22 A Reminder Utility Windows and Mac users have appreciated simple utilities like Stickies for years, the streamlined applications that let you keep tiny notes and reminders stuck on your screen. They’re perfect for jotting down phone numbers or other reminders. Unfortunately, there’s no analog if you want to take notes while working on a Unix command line, but the problem is easily solved with this pair of scripts. The first script, remember (shown in Listing 3-1), lets you easily save your snippets of information into a single rememberfile in your home directory. If invoked without any arguments, it reads standard input until the end-of-file sequence (^D) is given by pressing CTRL-D. If invoked with arguments, it just saves those arguments directly to the data file. The other half of this duo is remindme, a companion shell script shown in Listing 3-2, which either displays the contents of the whole rememberfile when no arguments are given or displays the results of searching through it using the arguments as a pattern.
#
#Taylor, Dave. Wicked Cool Shell Scripts, 2nd Edition (p. 80). No Starch Press. Kindle Edition. 
#
#First Thoughts: One bite at a time. Lets find out what the prompt is #asking.
#
#What do I need to do?
#I need to make a reminder utility.
#
#To do this I need two scripts
#1. remember.sh - saves your snippets of information into a single rememberfile in your home directory. If no arguments are given then it will wait for input until one is given or CTRL-D is pressed.
#
#2.remindme companion shell script which either displays the contents of the whole rememberfile when no arguments are given or displays the results of searching through it using the arguments as a pattern.
#
#
#
#OK first off I don't want to write two scripts and I already named this
#file. So I am going to derail this and do something different. 
#I'm just going to make it all in one file and take liberty to change anything else along the way. It would be cool to make a smaller termial open with your reminder on it, like sticky note.
#
#ALGORITHM
#1. The user will enter the script with the reminder they want to add
#2. If they entered a reminder then all past reminders will be displayed along with the new one added. A prompt will appear and ask if they wish to add or remove a reminder.
#If they do not enter reminder, then a prompt will appear asking them if #they want to add or remove a reminder. All past reminders that have been entered will be displayed.
#
#If user wants to add another one they can enter one. otherwise they can remove one.
###################################################################
#The user is free to leave by using their Get-Out-Of_Program CTRL-D Card.
#When the user leaves the reminder terminal closes. While it would be cool, Idk if I know how I would make the reminder terminal stay up in the top right corner of the screen even without the actual terminal open. Like sticky notes.
####################################################################
#PSEUDO CODE
#if user types CTRL-D 
#	then close the script
#else
#set arg1 as add_reminder variable
#add arg1 to reminder file
#open up a terminal window which displays all reminders in the reminder file
#ask user if they would like to add or remove a reminder
#if user types add
#	they will be prompted to type the new reminder
#if user types remove
#	They will be prompted to indicate which reminder to delete
#the reminder terminal will then update with the new updated data
#########################################################################
#PSEUDO CODE 2.0
#Ok in the pseudo code I wrote the whole thing in an if loop. I don't think I need that.
#arg1 = $1
#memories.txt += arg1
#display memories.txt
#
#read -p "Would you like to Add or Remove a reminder?(a/r)" reminder_temp
#
#Make reminder_temp all lowercase
#
#if (reminder_temp = (add || ad || a))
#	memories.txt += reminder_temp
#	display memories.txt
#	echo("Your reminder has been added")
#
#else if (reminder_temp = (remove || remov || remo || rem || re || r))
#	memories.txt -= reminder_temp
#	display memories.txt
#	echo("Your reminder has been removed")
#
#else
#	echo("Invalid entry. If you wish to close the program. Press CTRL-D")
##########################################################################
#Thinking about it more the loop is kind of stupid.
#I don't want the loop. I will get rid of reminder_temp and chage it to R_T
#Looks like its time fore 
#########################################################################
#PSEUDO CODE 3.0
#
#arg1 = $1
#memories.txt += arg1
#display memories.txt
#read -p "Would you like to Add or Remove a reminder?(a/r)" R_T
#
#R_T = ${R_T,,}
#
#case $R_T in
#
# 	add | ad | a)
#		memories.txt += R_T
#		display memories.txt
#		echo("Your reminder has been added")
#		;;
#
# 	remove | remov | remo | rem | re | r)
#		memories.txt += R_T
#		display memories.txt
#		echo("Your reminder has been added")
#		;;
#	*)
# 	echo -n "Invalid Input"
#esac
##########################################################################
#Need to find actually coud to do the line. Display memories.txt
#########################################################################	 
#PSEUDO CODE 4.0
#
#memories = "$HOME/.memories"
#
#arg1 = $1
#arg1 >> memories
#x-terminal-emulator -e memories
#
#read -p "Would you like to Add or Remove a reminder?(a/r)" R_T
#
#R_T = ${R_T,,}
#
#case $R_T in
#
# 	add | ad | a)
#		$R_T >> $memories
#		x-terminal-emulator -e $memories
#		echo("Your reminder has been added")
#		;;
#
# 	remove | remov | remo | rem | re | r)
#		sed -i /$R_T/d  file.txt
#		x-terminal-emulator -e $memories
#		echo("Your reminder has been removed")
#		;;
#	*)
# 	echo -n "Invalid Input"
#esac
#########################################################################
#
#########################################################################	 

#########################################################################
#First attempt at running program. I suck at this.
#########################################################################
#
#memories = "$HOME/.memories"#
#
#arg1 = $1
#arg1 > $memories
#x-terminal-emulator -e $memories#
#
#read -p "Would you like to Add or Remove a reminder?(a/r)" R_T#
#
#R_T = ${R_T,,}
#
#case $R_T in#
#
# 	add | ad | a)
#		$R_T >> $memories
#		x-terminal-emulator -e $memories
#		echo("Your reminder has been added")
#		;;
#
# 	remove | remov | remo | rem | re | r)
#		sed -i /$R_T/d  file.txt
#		x-terminal-emulator -e $memories
#		echo("Your reminder has been removed")
#		;;
#	*)
# 	echo -n "Invalid Input"
#esac
#########################################################################	 
#Trying out gnome-terminal. I don't think a separte terminal is how I want to display this but the idea is interesting. I'm going to try and learn a little about gnome-terminal.
##############################################################
#memories="$HOME/.memories"
#
#arg1="'$1'"
#$arg1 > $memories
#gnome-terminal --geometry=80x24+200+200 -e "bash -c \"cat $memories; exec #bash\" "###
#
#read -p "Would you like to Add or Remove a reminder?(a/r)" R_T##
#
#R_T=${R_T,,}
#
#case $R_T in
#
# 	add | ad | a)
# 		read -p "Type your memory to add" A_M
#		$A_M >> $memories
#		gnome-terminal -- cat '$memories'
#		echo "Your reminder has been added"
#		;;
#
# 	remove | remov | remo | rem | re | r)
# 	 		read -p "Type your memory to add" R_M
#
#		sed -i /$R_M/d  file.txt
#		gnome-terminal -- cat $memories
#		echo "Your reminder has been removed"
#		;;
#	*)
# 	echo "Invalid Input" ;;
#esac
#########################################################################	 
#Well that was a waste of time. Not really but I want to change everything.
#I just want to display in one terminal after all.
#########################################################################
#memories="./memories.txt"
#
#arg1="$@"
#echo "$@" >> $memories
#seeMemories=$(cat memories)
#echo "$seeMemories"
#read -p "Would you like to Add or Remove a reminder?(a/r)" R_T
#
#R_T=${R_T,,}
#
#case $R_T in
#
#	add | ad | a)
#		read -p "Type your memory to add: " A_M
#		["$A_M" >> "$memories" ]
#		echo "$seeMemories"
#		echo "Your reminder has been added"
#		;;
#
# 	remove | remov | remo | rem | re | r)
# 	 	read -p "Type your memory to remove: " R_M
#		sed -i /$R_M/d  $memories
#		echo "$seeMemories"
#		echo "Your reminder has been removed"
#		;;
#	*)
# 	echo "Invalid Input" ;;
#esac
#########################################################################	 
#Ok Slowly I am learning. 
#
#This code below works. But it has a few issues.
# FIXED WITH IF STATEMENT: 1. if run with no argument it adds a blank reminder
# 2. Removing reminders removes every reminder with the pattern of the input in it. 
#	EX: "This is a temp reminder" "This is a perm reminder" will both delete if the user inputs "t" for the remove case.
# 3. It ends after running once. The book promt asks for it to end when CTRL-D is entered.
# 4. I don't see issue 3 as an issue. lol.
#########################################################################
#memories="./memories.txt" #Make a file to store reminders.
#if $1 #if there is anything in the arguments
#then
#	echo "$@" >> $memories #Save the arguments as a Reminder.
#fi #end if statement.
#
#echo "$(cat ./memories.txt)" #Show all current Reminders
#read -p "Would you like to Add or Remove a reminder?(a/r)" R_T #prompt for action.
#R_T=${R_T,,} #Makes whatever entered lower case
#
#case $R_T in  #case command to determin action based on entery 
#
#	add | ad | a) #add case
#		read -p "Type your memory to add: " A_M #prompt for action.
#		echo "$A_M" >> "$memories" #Save user input to reminder file.
#		echo "$(cat ./memories.txt)" #Show all current Reminders
#		echo "Your reminder has been added" #Confirm for user
#		;;
#
 #	remove | remov | remo | rem | re | r) #remove case
 #	 	read -p "Type your memory to remove: " R_M #prompt for action.
#		sed -i /$R_M/d  $memories #This removes every line that matches to the input.
#		echo "$(cat ./memories.txt)" #Show all current Reminders
#		echo "Your reminder has been removed" #Confirm for user
#		;;
#	*)
# 	echo "Invalid Input" ;; #case for any input not add or remove. Will end the script.
#esac #end $R_T case.
#################################################################
#Not enough. want to add a search option. and make the remove better.
#
#maybe dont display whole file.
#have 4 options.
#display, search, add and remove.
#That sounds good :)
#################################################################
#memories="./memories.txt" #Make a file to store reminders.
#if [ "$@" ] #if there is anything in the arguments
#then
#	echo "$@" >> $memories #Save the arguments as a Reminder.
#fi #end if statement.
#
#echo "$(cat ./memories.txt)" #Show all current Reminders
#read -p "Would you like to Add, Display, Search, or Remove a reminder?(a/D/s/r)" R_T #prompt for action.
#R_T=${R_T,,} #Makes whatever entered lower case
#
#case $R_T in  #case command to determin action based on entery 
#
#	add | ad | a) #add case
#		read -p "Type your memory to add: " A_M #prompt for action.
#		echo "$A_M" >> "$memories" #Save user input to reminder file.
#		#echo "$(cat ./memories.txt)" #Show all current Reminders
#		echo "Your reminder has been added" #Confirm for user
#		;;
#		
#	display | displa | displ | disp | dis | di | d) #add case
#		echo "$(cat ./memories.txt)" #Show all current Reminders
#		;;
#
 #	search | searc | sear | sea | se  | s) #remove case
 #	 	read -p "Type your memory to search: " R_M #prompt for action.
#		echo "$(cat ./memories.txt)" | grep -P $R_M #This shows every line that matches to the input.
#		 #echo "Your reminder has been searched" #Confirm for user
#		;;
#
 #	remove | remov | remo | rem | re | r) #remove case
 #	 	read -p "Type your memory to remove: " R_M #prompt for action.
#		echo "$(cat ./memories.txt)" | sed /$R_M/d  #This removes every line that matches to the input.
#		 #Show all current Reminders
#		echo "Your reminder has been removed" #Confirm for user
#		;;
#	*)
#		echo "$(cat ./memories.txt)" #Show all current Reminders
#		;;
 	#echo "Invalid Input" ;; #case for any input not add or remove. Will end the script.
#esac #end $R_T case.
#################################################################
#
#I think this works well enough for the assignment.
#I want the remove to work better.
#################################################################
memories="./memories.txt" #Make a file to store reminders.
if [ "$@" ] #if there is anything in the arguments
then
	echo "$@" >> $memories #Save the arguments as a Reminder.
fi #end if statement.

#echo "$(cat ./memories.txt)" #Show all current Reminders
read -p "Would you like to Add, Display, Search, or Remove a reminder?(a/D/s/r)" R_T #prompt for action.
R_T=${R_T,,} #Makes whatever entered lower case

case $R_T in  #case command to determin action based on entery 

	add | ad | a) #add case
		read -p "Type your memory to add: " A_M #prompt for action.
		echo "$A_M" >> "$memories" #Save user input to reminder file.
		echo "Your reminder has been added" #Confirm for user
		;;
		
	display | displa | displ | disp | dis | di | d) #add case
		echo "$(cat ./memories.txt)" #Show all current Reminders
		;;

 	search | searc | sear | sea | se  | s) #remove case
 	 	read -p "Type your memory to search: " R_M #prompt for action.
		echo "$(cat ./memories.txt)" | grep -P $R_M #This shows every line that matches to the input.
		;;

 	remove | remov | remo | rem | re | r) #remove case
 	 	read -p "Type your memory to remove: " R_M #prompt for action.
		echo "$(cat ./memories.txt)" | sed /$R_M/d  #This removes every line that matches to the input.
		echo "Your reminder has been removed" #Confirm for user
		;;
	*)
		echo "$(cat ./memories.txt)" #Show all current Reminders
		;;

esac #end $R_T case.




































