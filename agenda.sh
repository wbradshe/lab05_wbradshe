#!/bin/bash
######################################################################
#algorithm

#addagenda
#-enables you to specify a recurring event
#-(with either a day of the week for weekly events or a day and month for annual ones) 
#-or a one-time event (with the day, month, and year).
#-All the dates are validated and saved, along with a one-line event description, in an .agenda file in your home directory.


#agenda
#-checks all known events to show which ones are scheduled for the current date.
#######################################################################
#
#
#
#agenda
#	get date
#	
#	echo "Your One-Time reminders:"
#	grep all line of the file with S in them and the current date
#
#	
#	echo "Your Weekly reminders:"
#	grep all lines with W and the day of week in them
#	
#	
#	
#	echo "Your annual reminders:"
#	grep all lines with A and 
#######################################################################
#agenda
	#agenda file import
	#agendaFile=$(<agendaFile.txt)
	#echo <agendaFile.txt
	echo
	echo
	#get date #example date: wed 04/20/2022
	dayCheck=$(date +"%d") #example output 20
	monthCheck=$(date +"%m") #example output 04
	yearCheck=$(date +"%y") #example output 22
	wDayCheck=$(date +"%a" | tr '[:upper:]' '[:lower:]') #example output wed
	
	#echo $dayCheck
	#echo $monthCheck
	#echo $yearCheck
	#echo $wDayCheck
	
	echo "Your One-Time reminders: "
	#grep all lines of the file with O in them and the current date
	grep "O" agendaFile.txt | grep "$yearCheck $monthCheck $dayCheck" | cut -d " " -f 6- 
	echo
	echo
	
	echo "Your Weekly reminders: "
	#grep all lines with W and the day of week in them
	grep "W" agendaFile.txt | grep "$wDayCheck" | cut -d " " -f 3- 
	echo
	echo	
	
	echo "Your annual reminders: "
	#grep all lines with A and 
	grep "A" agendaFile.txt | grep "$dayCheck $monthCheck" | cut -d " " -f 4-
	echo
	echo

