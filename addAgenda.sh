#!/bin/bash
#26 Keeping Track of Events
#This is actually a pair of scripts that together implement a simple calendar program, similar to our reminder utility from Script #22 on page 80. The first script, addagenda (shown in Listing 3-12), enables you to specify a recurring event (with either a day of the week for weekly events or a day and month for annual ones) or a one-time event (with the day, month, and year). All the dates are validated and saved, along with a one-line event description, in an .agenda file in your home directory. The second script, agenda (shown in Listing 3-13), checks all known events to show which ones are scheduled for the current date. This kind of tool is particularly useful for remembering birthdays and anniversaries. If you have trouble remembering events, this handy script can save you a lot of grief!
######################################################################
#algorithm

#addagenda
#-enables you to specify a recurring event
#-(with either a day of the week for weekly events or a day and month for annual ones) 
#-or a one-time event (with the day, month, and year).
#-All the dates are validated and saved, along with a one-line event description, in an .agenda file in your home directory.


#agenda
#-checks all known events to show which ones are scheduled for the current date.
#######################################################################
#Pseudo Code
#
#addAgenda
	#prompt for if its a one-time, weekly, or annual event
	#prompt for an event discription
#	
#	case eventType
#		Singal(one-Time))
#			prompt for what day, month, and year to repeat once
#			save info in an array formated (S yy mm dd eventDiscription)
		
#		weekly)
#			prompt for (mon, tue, wed, thur, fri, sat, sun)
#			save info in an array formated (W dayOfWeek eventDiscription)
#		
#		annual)
#			prompt for what Day and Month
#			save info in an array formated (A mm dd eventDiscription)
#

#######################################################################
#addAgenda

	agendaFile="./agendaFile.txt" #Make a agendaFile.

	echo "Hello welcome to addAgenda"
	#prompt for if its a one-time, weekly, or annual event
	read -n1 -p "Would you like to add an One-time, Weekly, or Annual Event?(O/w/a): " eventType
	
	eventType=$(echo $eventType | tr '[:upper:]' '[:lower:]')
	echo


	#asks for what info is needed based on eventType. Checks if correct letter was inputed.

	case $eventType in
		o)
			#prompt for what day, month, and year to repeat once
			read -n2 -p "Please enter two digits for the day (EX: January, 09 2022 would be 09): " oDay
			echo
			read -n2 -p "Please enter two digits for the month (EX: January would be 01): " oMonth
			echo
			read -n2 -p "Please enter two digits for the year (EX: 2022 would be 22): " oYear
			echo
			
			#prompt for an event discription
			read -p "Please type out the event discription: " eventDiscription
			echo


			#save info in agenda file, formating: (O yy mm dd eventDiscription)
			echo "O $oYear $oMonth $oDay $eventDiscription" >> "$agendaFile"
			;;
		
		w)
			#prompt for (mon, tue, wed, thu, fri, sat, 				sun)
			echo "Please enter 3 letters for the day of week you want this reminder to repeat" 
			
			
			read -n3 -p "example inputs (mon, tue, wed, thu, fri, sat, sun): " wDay
			echo

			wDay=$(echo $wDay | tr '[:upper:]' '[:lower:]')


			#prompt for an event discription
			read -p "Please type out the event discription: " eventDiscription
			echo




			#save info in agenda file, formating:  (W dayOfWeek eventDiscription)
			echo "W $wDay $eventDiscription" >> "$agendaFile"
			;;

		
		a)
			#prompt for what Day and Month
			read -n2 -p "Please enter two digits for the day: " aDay
			echo
			read -n2 -p "Please enter two digits for the month: " aMonth
			echo



			#prompt for an event discription
			read -p "Please type out the event discription: " eventDiscription
			echo



			#save info in agenda file, formating: (A mm dd eventDiscription)
			echo "A $aDay $aMonth $eventDiscription" >> "$agendaFile"
			;;
		*)
			echo "Invalid Entry. Please Try again"
			exit 1
			;;
	esac






